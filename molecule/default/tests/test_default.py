import os
import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('pdns_zone')


def test_pdns_package(host):
    p = host.package("pdns")
    assert p.is_installed
